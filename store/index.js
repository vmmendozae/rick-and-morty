export const state = () => ({ // data
    selectedCharacter: {
        name: 'Inicial'
    },
    characters: [],
    episodes: [],
    pageCharacters: 1,
    pageEpisodes: 1,
})

export const getters = { // computed
    getCounter(state) {
        return state.counter
    }
}

export const mutations = { // para modificar el estado => commit
    selectCharacter(state, character) {
        state.selectedCharacter = character
    },
    changeCharacters(state, characters) {
        state.characters = characters
    },
    changeEpisodes(state, episodes) {
        state.episodes = episodes
    },
    changePageCharacters(state, page) {
        state.pageCharacters = page
    },
    changePageEpisodes(state, page) {
        state.pageEpisodes = page
    }
}

export const actions = { // methods => dispatch
    async fetchCharacters({ commit, state }) {
        let response = await this.$axios.get(
            `https://rickandmortyapi.com/api/character?page=${state.pageCharacters}`
        );
        commit('changePageCharacters', state.pageCharacters + 1)
        commit('changeCharacters', [...state.characters, ...response.data.results])
    },
    async fetchEpisodes({ commit, state }) {
        let response = await this.$axios.get(
            `https://rickandmortyapi.com/api/episode?page=${state.pageEpisodes}`
        );
        commit('changePageEpisodes', state.pageEpisodes + 1)
        commit('changeEpisodes', [...state.episodes, ...response.data.results])
    }
}